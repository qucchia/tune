\version "2.22.0"

\header {
  title = "Tune"
  composer = "qucchia"
}

piano = \relative c'' {
	\clef "treble"
	\key c \major
	\time 4/4
	\tempo "Andante" 4 = 120
	
  r1 | r1 | r1 | r1 |
  r1 | r1 | r1 | r1 |
  c2.\f c8 d | ees4. f g8 aes~ | aes1 | aes4. bes ces8 c~ |
  c2 c, | d4. ees bes'8 a~ | a1 | a4. g f8 g~ |
  g2. c,8 d | ees4. f g8 aes~ | aes1 | aes4. bes ces8 c~ |
  c2 c, | d4. ees bes'8 a~ | a1 | a4. g f8 g~ |
}

bassNotes = {
  dis8\pp c d c dis c d c | dis c d c dis c d c |
  dis ces des ces dis ces des ces | dis ces des ces dis ces des ces |
  dis8 c d c dis c d c | dis c d c dis c d c |
  eis8 c d c eis c d c | eis c d c eis c d c |
}

bass = \relative c {
  \clef "bass"
	\key c \major
	\time 4/4
	\tempo "Andante" 4 = 120
  \bassNotes \bassNotes \bassNotes
}

\score {
  <<
    \new Staff \piano
    \new Staff \bass
  >>
	\layout { }
	\midi { }
}
